const functions = require('firebase-functions');


var admin = require("firebase-admin");

var serviceAccount = require("./fir-demo.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://fir-demo-221b9.firebaseio.com"
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.obtenerDatos = functions.firestore.document("compras/{compraId}")
.onCreate(async(snapshot, context) => {
 const itemDataSnapshot = await snapshot.ref.get();
 console.log("El email del usuario es:" + itemDataSnapshot.data().email);
 return admin.firestore().collection('mail').add({
    to: [itemDataSnapshot.data().email],
    message: {
      subject: 'Gracias por su compra!',
    //   text: 'This is the plaintext section of the email body.',
      html: `Hola ${itemDataSnapshot.data().nombre} gracias por tu compra por 
      ${itemDataSnapshot.data().precio}, tu reserva fue agendada`,
    }
  }).then(() => console.log('Queued email for delivery!'));
});
